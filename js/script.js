/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
/*! NOTE: If you're already including a window.matchMedia polyfill via Modernizr or otherwise, you don't need this part */
window.matchMedia=window.matchMedia||function(a){"use strict";var c,d=a.documentElement,e=d.firstElementChild||d.firstChild,f=a.createElement("body"),g=a.createElement("div");return g.id="mq-test-1",g.style.cssText="position:absolute;top:-100em",f.style.background="none",f.appendChild(g),function(a){return g.innerHTML='&shy;<style media="'+a+'"> #mq-test-1 { width: 42px; }</style>',d.insertBefore(f,e),c=42===g.offsetWidth,d.removeChild(f),{matches:c,media:a}}}(document);

$(document).ready(function(){

  $('html').removeClass('no-js').addClass('js'); // Enable .js styles

  $('.listPhotos li a').fancybox(); // Bind lightbox

  // Check media
  if (matchMedia('only screen and (max-width: 330px)').matches) {
    // INVOKE TINY NAV!
    $("#nav")
      .hide()
      .tinyNav({
        active: 'is-current', // String: Set the "active" class
        label: 'Navigation' // String: Sets the <label> text for the <select> (if not set, no label will be added)
      });
  }

});

$(window).load(function() {

  $('.flexslider').flexslider({
    controlNav: false,
    directionNav: false,
    slideshowSpeed: 4000
  });

});
